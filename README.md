# no_spiders

disable spiders in mcl2


this is currently done by reducing the chance of spawning spiders.

## Issues
- I have yet to figure out how to stop the spawning of spiders completely.
- I have no idea, what spider eggs do.


## References

- mob api: https://repo.or.cz/MineClone/MineClone2.git/blob_plain/HEAD:/mods/ENTITIES/mcl_mobs/api.txt
- minetest dev wiki: https://dev.minetest.net/Main_Page
- mineclone2 thread: https://forum.minetest.net/viewtopic.php?t=16407
- 

